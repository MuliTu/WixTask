import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import './Image.scss';
import {getImageFromFlicker} from 'components/Image/ImageActions';

class Image extends React.Component {
  static propTypes = {
    dto: PropTypes.object,
    galleryWidth: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.calcImageSize = this.calcImageSize.bind(this);
    this.state = {
      rotate: 0,
      isSelected: false
    };
  }

  calcImageSize() {
    const {galleryWidth} = this.props;
    const targetSize = 200;
    const imagesPerRow = Math.round(galleryWidth / targetSize);
    return (galleryWidth / imagesPerRow)
  }

  componentDidMount() {
    this.calcImageSize();
  }

   urlFromDto(dto) {
    return getImageFromFlicker(dto)
  }

  rotateImage = ()=> {
    return {
      backgroundImage: `url(${this.urlFromDto(this.props.dto)})`,
      width: `${this.calcImageSize()}px`,
      height: `${this.calcImageSize()}px`,
      transform: `rotate(${this.state.rotate}deg)`,
      transition: 'transform 1s'
    };
  };

  rotate() {
    this.setState({
      rotate: this.state.rotate + 90
    })
  }

  render() {
    return (
      <div
        draggable
        className="image-root"
        style={this.rotateImage()}>

        <div style={{transform: `rotate(-${this.state.rotate}deg)`}}>
          <FontAwesome className="image-icon" name="sync-alt" title="rotate" onClick={() => {
            this.rotate()
          }}/>
          <FontAwesome className="image-icon" name="trash-alt" title="delete" onClick={() => {
            this.props.delete(this.props.dto)
          }}/>
          <FontAwesome className="image-icon" name="expand" title="expand" onClick={() => {
            this.props.expand(this.props.dto)
          }}/>
        </div>

      </div>
    );
  }
}

export default Image;
