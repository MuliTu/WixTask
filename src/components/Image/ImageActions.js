import React from 'react'
import FontAwesome from 'react-fontawesome';

export const ZoomInImage = (props) => {
  return (
    <div className={'selected-image'}>
      <ExitButton action={props.close}/>
      <div className={'image-module'}>
        <ArrowButton name={'angle-left'} title={'expand'} action={() => props.iterator('prev')}/>
        <MyImage data={props.data}/>
        <ArrowButton name={'angle-right'} title={'expand'} action={() => props.iterator('next')}/>
      </div>
    </div>)
};

export const getImageFromFlicker = (image) => {
  return `https://farm${image.farm}.staticflickr.com/${image.server}/${image.id}_${image.secret}.jpg`
};

export const MyImage = ({data}) => {
  return <div className={'image'}>
    <div className={'image-context'}>{data.title}</div>
    <img className={'image-fix'} src={getImageFromFlicker(data)}>
    </img></div>

};

export const ExitButton = ({action}) => {
  return (<div className={'button'}>
    <FontAwesome name={'times-circle'}
                 onClick={action}/>

  </div>)
};

export const ArrowButton = ({name, title, action}) => {
  return (
    <FontAwesome className="arrow" name={name} title={title} onClick={action}/>)
};

