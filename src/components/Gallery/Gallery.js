import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Image from '../Image';
import './Gallery.scss';
import FontAwesome from 'react-fontawesome';
import {ZoomInImage} from 'components/Image/ImageActions';
import TrashZone from 'components/Gallery/TrashZone';
import DraggableZone from 'components/Gallery/DraggableZone';

class Gallery extends React.Component {
  static propTypes = {
    tag: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      images: [],
      galleryWidth: this.getGalleryWidth(),
      selectedImage: null,
      amount: 100,
      drag:false
    };
  }

  getGalleryWidth() {
    try {
      return document.body.clientWidth;
    } catch (e) {
      return 20;
    }
  }

  getImages(tag) {
    const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad&tags=${tag}&tag_mode=any&per_page=${this.state.amount}&format=json&safe_search=1&nojsoncallback=1`;
    const baseUrl = 'https://api.flickr.com/';
    axios({
      url: getImagesUrl,
      baseURL: baseUrl,
      method: 'GET'
    })
      .then(res => res.data)
      .then(res => {
        if (
          res &&
          res.photos &&
          res.photos.photo &&
          res.photos.photo.length > 0
        ) {
          this.setState({images: res.photos.photo});
        }
      });
  }

  componentDidMount() {
    this.getImages(this.props.tag);
    this.windowListener()
  }

  windowListener = () => {
    window.addEventListener('resize', () => {
      this.setState({
        galleryWidth: document.body.clientWidth
      });
    });
    window.addEventListener('scroll', () => {
      this.onScroll();
    })
  };

  onScroll = () => {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      this.setState({
        amount: this.state.amount + 10
      });
      this.getImages()
    }
  };

  componentWillReceiveProps(props) {
    this.getImages(props.tag);
  }

  openExpand = (data) => {
    let currentIndex = this.state.images.indexOf(data);
    this.setState({
      selectedImage: this.state.images[currentIndex]
    });
  };

  closeExpand = () => {
    this.setState({
      selectedImage: null
    })
  };

  currentImageIterator = (data) => {
    let currentIndex = this.state.images.indexOf(this.state.selectedImage);
    if (currentIndex < this.state.images.length - 1 && currentIndex >= 1) {
      let currentImage = (data === 'next' ? this.state.images[currentIndex + 1] : this.state.images[currentIndex - 1]);
      this.setState({
        selectedImage: currentImage
      })
    }
  };

  deleteImageFromList = (image) => {
    let index = (this.state.images.indexOf(image));
    let updatedList = this.state.images;
    updatedList.splice(index, 1);
    this.setState({
      images: updatedList
    })
  };

  updateState = (data) => {
    this.setState({
      images: data
    })
  };

  onDragStart = (e, index) => {
    this.setState({
      drag:true
    });
    const stringifyIndex = JSON.stringify(index);
    e.dataTransfer.setData('index', stringifyIndex);
    e.dataTransfer.effectAllowed = 'move'
  };

  onDragOver = (e) => {
    e.preventDefault();
  };

  indexToImage = (index) => {
    this.change(false);
    const tempImage = this.state.images[index];
    this.deleteImageFromList(tempImage);
  };

  change(bool){
    this.setState({
      drag:bool
    })
  }

  render() {
    return (
      <div className="gallery-root">
        {this.state.drag === true ?
          <TrashZone
            onDragOver={this.onDragOver}
            onDragStart={this.onDragStart}
            delete={this.indexToImage}/> : null
        }

        {this.state.images.map((dto, index) => {
          return <DraggableZone key={index}
                                index={index}
                                updateState={(data)=>{this.updateState(data)}}
                                delete={(image)=>{this.deleteImageFromList(image)}}
                                data={this.state.images}
                                isDragged={(bool)=>this.change(bool)}>
            <Image key={'image-' + dto.id}
                   dto={dto}
                   galleryWidth={this.state.galleryWidth}
                   expand={this.openExpand}
                   delete={this.deleteImageFromList}/>
          </DraggableZone>
        })}
        <div className={'selected-image-wrapper'}>
          {
            this.state.selectedImage !== null ?
              <ZoomInImage data={this.state.selectedImage}
                           close={this.closeExpand}
                           iterator={(data) => {
                             this.currentImageIterator(data)
                           }}/>

              :
              <FontAwesome className={'loading-icon'} name='spinner'/>
          }
        </div>
      </div>
    );
  }
}

export default Gallery;


