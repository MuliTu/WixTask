import React from 'react';

export class DraggableZone extends React.Component {
  constructor(props) {
    super(props);
  }

  onDragOver = (e) => {
    e.preventDefault();

  };

  onDragStart = (e, index) => {
    this.props.isDragged(true);
    const stringifyIndex = JSON.stringify(index);
    e.dataTransfer.setData('index', stringifyIndex);
    e.dataTransfer.effectAllowed = 'move'
  };

  swap = (y, x) => {
    this.props.isDragged(false);
    let tempList = this.props.data;
    let b = tempList[y];
    tempList[y] = tempList[x];
    tempList[x] = b;
    this.props.updateState(tempList);

  };



  onDrop = (e, target) => {
    let currentIndex = JSON.parse(e.dataTransfer.getData('index'));
    this.swap(currentIndex, target);
    e.dataTransfer.dropEffect = 'move';
  };

  render() {
    return(<div className='fix'
                 draggable
                 onDrop={(e) => {this.onDrop(e, this.props.index);}}
                 onDragStart={(e) => {this.onDragStart(e, this.props.index);}}
                 onDragOver={(e) => {this.onDragOver(e);}}>
        {this.props.children}

      </div>
    )
  }
}

export default DraggableZone
