import React from 'react'
import FontAwesome from 'react-fontawesome';

class TrashZone extends React.Component {
  constructor(props) {
    super(props);
  }
  dropOnTrashZone = (e) => {
    let currentIndex = JSON.parse(e.dataTransfer.getData('index'));
    this.props.delete(currentIndex);
  };

  render() {
    return (<div className={'trash-zone'}
                 onDrop={(e) => {
                   this.dropOnTrashZone(e)
                 }}
                 onDragStart={(e) => {
                   this.props.onDragStart(e)
                 }}
                 onDragOver={(e) => {
                   this.props.onDragOver(e)
                 }}>
      <FontAwesome className="image-icon" name="trash-alt" title="delete"/>
    </div>);
  }
}

export default TrashZone
